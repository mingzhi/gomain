package main

import (
	"bitbucket.org/mingzhi/hgt"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
)

type Result struct {
	Ks, Vard                           float64
	Scovs, Rcovs, XYPL, XsYsPL, SmXYPL []float64
}

// population parameters
var size, length, fragment int
var mutation, transfer float64

// sample parameters
var sampleSize, startIndex, replicate, generations, maxL int
var prefix, outdir string
var fixed bool

func init() {
	// bind flags
	flag.IntVar(&size, "size", 1000, "population size")
	flag.IntVar(&length, "genome", 1000, "genome length")
	flag.IntVar(&fragment, "frag", 100, "fragment length")
	flag.Float64Var(&mutation, "mutation", 1e-4, "mutation rate")
	flag.Float64Var(&transfer, "transfer", 0, "transfer rate")
	flag.IntVar(&sampleSize, "sample", 100, "sample size")
	flag.IntVar(&startIndex, "index", 1, "start index")
	flag.IntVar(&replicate, "num", 100, "number of replicates")
	flag.IntVar(&generations, "gen", 10000, "number of generations")
	flag.IntVar(&maxL, "maxl", 100, "maxL")
	flag.StringVar(&prefix, "prefix", "test", "prefix")
	flag.StringVar(&outdir, "out", "out", "output folder")
	flag.BoolVar(&fixed, "fixed", false, "fixed selection time")
	flag.Parse()
}

func main() {
	// runing parameters
	ncpu := runtime.NumCPU()
	runtime.GOMAXPROCS(ncpu)

	// create job queens
	jobQueen := make(chan int, ncpu)
	go func(begin, num int, queen chan int) {
		for i := begin; i < begin+num; i++ {
			queen <- i
		}
		close(jobQueen)
	}(startIndex, replicate, jobQueen)

	// start workers
	resQueen := make(chan Result, ncpu)
	for i := 0; i < ncpu; i++ {
		go func(in chan int, out chan Result) {
			for seed := range in {
				res := run(seed)
				out <- res
			}
		}(jobQueen, resQueen)
	}

	for i := 0; i < replicate; i++ {
		res := <-resQueen
		filename := fmt.Sprintf("%s/%s_%d.json", outdir, prefix, startIndex+i)
		out, err := os.Create(filename)
		if err != nil {
			log.Panicf("error when opeing file %s: %v\n", filename, err)
		}

		content, err := json.Marshal(res)
		if err != nil {
			log.Panicf("error when marshal result to json: %v\n", err)
		}
		out.Write(content)
		out.Close()

		log.Printf("Done %d\n", startIndex+i)
	}
}

func run(seed int) (res Result) {
	p := hgt.NewPopulation(size, length)
	p.MutationRate = mutation
	p.TransferRate = transfer
	p.FragmentLength = fragment
	p.FixedTime = fixed

	p.Seed(seed)

	letters := "ATGC"
	p.CreateGenomes(letters)

	p.Evolve(generations)

	matrix := p.Sample(sampleSize)
	dmatrix, err := hgt.NewDMatrix(matrix)
	if err != nil {
		panic(err)
	}

	res.Ks, res.Vard = dmatrix.D()
	res.Scovs, res.Rcovs, res.XYPL, res.XsYsPL, res.SmXYPL = dmatrix.CovCircle(maxL)

	return
}

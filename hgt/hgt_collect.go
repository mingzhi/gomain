package main

import (
	"bitbucket.org/mingzhi/gomath/stat/desc"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
)

var dir string
var name string
var num int
var maxL int

type Result struct {
	Ks, Vard                           float64
	Scovs, Rcovs, XYPL, XsYsPL, SmXYPL []float64
}

func init() {
	flag.IntVar(&num, "num", 10000, "number of replicates")
	flag.StringVar(&dir, "dir", "out", "output folder")
	flag.Parse()
	if len(flag.Args()) < 1 {
		log.Panic("need parameter: name")
	}

	name = flag.Arg(0)
}

func main() {
	means := make([][]*desc.Mean, 5)
	sds := make([][]*desc.StandardDeviation, 5)

	dFile, err := os.Create(fmt.Sprintf("%s_d.csv", name))
	if err != nil {
		log.Fatalf("error when creating file %s, %v", name, err)
	}
	defer dFile.Close()

	for i := 0; i < num; i++ {
		filename := fmt.Sprintf("%s/%s_%d.json", dir, name, i)
		file, err := os.Open(filename)
		if err != nil {
			log.Printf("error when opening file %s, %v", filename, err)
			continue
		}
		var res Result
		dec := json.NewDecoder(file)
		if err := dec.Decode(&res); err != nil && err != io.EOF {
			log.Printf("error when decoding %v", err)
			continue
		}
		dFile.WriteString(fmt.Sprintf("%g,%g\n", res.Ks, res.Vard))

		if len(res.Scovs) != maxL {
			if i == 0 {
				maxL = len(res.Scovs)
				for i := 0; i < 5; i++ {
					means[i] = make([]*desc.Mean, maxL)
					sds[i] = make([]*desc.StandardDeviation, maxL)
					for j := 0; j < maxL; j++ {
						means[i][j] = desc.NewMean()
						sds[i][j] = desc.NewStandardDeviationWithBiasCorrection()
					}
				}
			} else {
				log.Printf("in %s, maxL is %d, but it should be %d\n", filename, len(res.Scovs), maxL)
				continue
			}
		}

		for l := 0; l < maxL; l++ {
			means[0][l].Increment(res.Scovs[l])
			means[1][l].Increment(res.Rcovs[l])
			means[2][l].Increment(res.XYPL[l])
			means[3][l].Increment(res.XsYsPL[l])
			means[4][l].Increment(res.SmXYPL[l])
			sds[0][l].Increment(res.Scovs[l])
			sds[1][l].Increment(res.Rcovs[l])
			sds[2][l].Increment(res.XYPL[l])
			sds[3][l].Increment(res.XsYsPL[l])
			sds[4][l].Increment(res.SmXYPL[l])
		}

		file.Close()
	}

	cFile, err := os.Create(fmt.Sprintf("%s_covs.csv", name))
	if err != nil {
		log.Fatalf("error when creating file %s, %v\n", name, err)
	}
	defer cFile.Close()

	for l := 0; l < maxL; l++ {
		cFile.WriteString(fmt.Sprintf("%d, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %d\n",
			l,
			means[0][l].GetResult(),
			means[1][l].GetResult(),
			means[2][l].GetResult(),
			means[3][l].GetResult(),
			means[4][l].GetResult(),
			sds[0][l].GetResult(),
			sds[1][l].GetResult(),
			sds[2][l].GetResult(),
			sds[3][l].GetResult(),
			sds[4][l].GetResult(),
			means[0][l].GetN(),
		))
	}
}

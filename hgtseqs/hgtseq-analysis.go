package main

import (
	"bitbucket.org/mingzhi/biogo"
	"bitbucket.org/mingzhi/biogo/bioutil"
	"bitbucket.org/mingzhi/gomath/stat/desc"
	"bitbucket.org/mingzhi/hgtseqs"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"strings"
)

type Result struct {
	Ks, VarD         float64
	XYs, XsYs, SmXYs []float64
}

type Stats struct {
	Mean *desc.Mean
	Sd   *desc.StandardDeviation
}

func NewStats() *Stats {
	s := Stats{}
	s.Mean = desc.NewMean()
	s.Sd = desc.NewStandardDeviationWithBiasCorrection()
	return &s
}

func (s *Stats) Increment(v float64) {
	s.Mean.Increment(v)
	s.Sd.Increment(v)
}

type ResultStats struct {
	MaxL                           int
	Ks, VarD                       *Stats
	XYs, XsYs, SmXYs, Scovs, Rcovs []*Stats
}

func NewResultStats(maxl int) *ResultStats {
	rs := ResultStats{}
	rs.Ks = NewStats()
	rs.VarD = NewStats()
	rs.XYs = make([]*Stats, maxl)
	rs.XsYs = make([]*Stats, maxl)
	rs.SmXYs = make([]*Stats, maxl)
	rs.Scovs = make([]*Stats, maxl)
	rs.Rcovs = make([]*Stats, maxl)

	rs.MaxL = maxl

	for i := 0; i < maxl; i++ {
		rs.XYs[i] = NewStats()
		rs.XsYs[i] = NewStats()
		rs.SmXYs[i] = NewStats()
		rs.Scovs[i] = NewStats()
		rs.Rcovs[i] = NewStats()
	}

	return &rs
}

func (rs *ResultStats) Increment(ks, vd float64, xyP, xsysP, smXYP []float64) {
	if !math.IsNaN(vd) {
		rs.Ks.Increment(ks)
		rs.VarD.Increment(vd)
	}

	for i := 0; i < rs.MaxL; i++ {
		xy := xyP[i]
		if !math.IsNaN(xy) {
			rs.XYs[i].Increment(xy)
		}
		xsys := xsysP[i]
		if !math.IsNaN(xsys) {
			rs.XsYs[i].Increment(xsys)
		}
		smxy := smXYP[i]
		if !math.IsNaN(smxy) {
			rs.SmXYs[i].Increment(smxy)
		}
		scov := xy - xsys
		if !math.IsNaN(scov) {
			rs.Scovs[i].Increment(scov)
		}
		rcov := xsys - smxy
		if !math.IsNaN(rcov) {
			rs.Rcovs[i].Increment(rcov)
		}
	}
}

func main() {
	var maxL, cutoff int
	var workspace string
	flag.IntVar(&maxL, "maxl", 1000, "maxL")
	flag.IntVar(&cutoff, "cutoff", 5, "cutoff")
	flag.StringVar(&workspace, "workspace", "/scratch/ml3365/genomes/Workspace", "workspace")
	flag.Parse()
	if flag.NArg() < 1 {
		log.Fatal("need species name")
	}
	species := flag.Arg(0)

	// read species infors
	speciesInforMap := hgtseqs.ReadSpeciesInfors("species_infors.csv")
	speciesCluster, found := speciesInforMap[species]
	if !found {
		log.Fatal("can not find species " + species)
	}

	// get codon table
	codeId := speciesCluster[0].CodeTable
	codeTables := bioutil.BuildinCodonTables()
	codeTable := codeTables[codeId]

	// check workspace
	if _, err := os.Stat(workspace); os.IsNotExist(err) {
		log.Fatalf("can not find workspace: %s", workspace)
	}

	// alignment json decoder
	alignDir := workspace + "/" + species + "/" + "/alignments/outs"
	nuclAlignFile, err := os.Open(alignDir + "/nucl_alignments.json")
	if err != nil {
		log.Fatalln(err)
	}
	defer nuclAlignFile.Close()

	decoder := json.NewDecoder(nuclAlignFile)

	// result stats
	resultStatMap := make(map[string]*ResultStats)

	for {
		var seqs []*biogo.SeqRecord
		if err := decoder.Decode(&seqs); err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		var ids, alignment []string
		for _, s := range seqs {
			ids = append(ids, s.Id)
			alignment = append(alignment, s.Seq.Data)
		}

		// filter
		alignment = hgtseqs.Filter(alignment)
		if len(alignment) >= cutoff {
			{
				profile, _ := hgtseqs.FFProfile(alignment, codeTable)
				distMatrix := hgtseqs.Compare(alignment, profile)
				ks, vd, xyP, xsysP, smXYP := hgtseqs.Cov(distMatrix, maxL)
				name := "FF"
				rs, found := resultStatMap[name]
				if !found {
					resultStatMap[name] = NewResultStats(maxL)
					rs = resultStatMap[name]
				}
				rs.Increment(ks, vd, xyP, xsysP, smXYP)
			}

			{
				profile := hgtseqs.PositionProfile(alignment, 0)
				distMatrix := hgtseqs.Compare(alignment, profile)
				ks, vd, xyP, xsysP, smXYP := hgtseqs.Cov(distMatrix, maxL)
				name := "First"
				rs, found := resultStatMap[name]
				if !found {
					resultStatMap[name] = NewResultStats(maxL)
					rs = resultStatMap[name]
				}
				rs.Increment(ks, vd, xyP, xsysP, smXYP)
			}

			{
				profile := hgtseqs.PositionProfile(alignment, 1)
				distMatrix := hgtseqs.Compare(alignment, profile)
				ks, vd, xyP, xsysP, smXYP := hgtseqs.Cov(distMatrix, maxL)
				name := "Second"
				rs, found := resultStatMap[name]
				if !found {
					resultStatMap[name] = NewResultStats(maxL)
					rs = resultStatMap[name]
				}
				rs.Increment(ks, vd, xyP, xsysP, smXYP)
			}

			{
				profile := hgtseqs.PositionProfile(alignment, 2)
				distMatrix := hgtseqs.Compare(alignment, profile)
				ks, vd, xyP, xsysP, smXYP := hgtseqs.Cov(distMatrix, maxL)
				name := "Third"
				rs, found := resultStatMap[name]
				if !found {
					resultStatMap[name] = NewResultStats(maxL)
					rs = resultStatMap[name]
				}
				rs.Increment(ks, vd, xyP, xsysP, smXYP)
			}

			{
				aaAlignment, _ := hgtseqs.Translate(alignment, codeTable)
				profile := make([]bool, len(aaAlignment[0]))
				for i := 0; i < len(profile); i++ {
					profile[i] = true
				}
				distMatrix := hgtseqs.Compare(aaAlignment, profile)
				ks, vd, xyP, xsysP, smXYP := hgtseqs.Cov(distMatrix, maxL)
				name := "AA"
				rs, found := resultStatMap[name]
				if !found {
					resultStatMap[name] = NewResultStats(maxL)
					rs = resultStatMap[name]
				}
				rs.Increment(ks, vd, xyP, xsysP, smXYP)
			}
		} else {
			log.Printf("To many gaps: %s\n", strings.Join(ids, "|"))
		}
	}

	// write result stats
	// result json output
	analysisDir := workspace + "/" + species + "/" + "/analysis/outs"
	if _, err := os.Stat(analysisDir); os.IsNotExist(err) {
		var perm os.FileMode = 0766
		if err := os.MkdirAll(analysisDir, perm); err != nil {
			log.Panic(err)
		}
	}

	for name, rs := range resultStatMap {
		filename := analysisDir + "/" + name + "_covs.csv"
		file, err := os.Create(filename)
		if err != nil {
			log.Panic(err)
		}
		defer file.Close()

		file.WriteString(fmt.Sprintf("#Ks = %g, %g, %d\n", rs.Ks.Mean.GetResult(), rs.Ks.Sd.GetResult(), rs.Ks.Mean.GetN()))
		file.WriteString(fmt.Sprintf("#VarD = %g, %g, %d\n", rs.VarD.Mean.GetResult(), rs.VarD.Sd.GetResult(), rs.VarD.Mean.GetN()))

		for i := 0; i < maxL; i++ {
			if rs.Scovs[i].Mean.GetN() > 0 {
				file.WriteString(fmt.Sprintf("%d, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %d\n",
					i,
					rs.XYs[i].Mean.GetResult(),
					rs.XsYs[i].Mean.GetResult(),
					rs.SmXYs[i].Mean.GetResult(),
					rs.Scovs[i].Mean.GetResult(),
					rs.Rcovs[i].Mean.GetResult(),
					rs.XYs[i].Sd.GetResult(),
					rs.XsYs[i].Sd.GetResult(),
					rs.SmXYs[i].Sd.GetResult(),
					rs.Scovs[i].Sd.GetResult(),
					rs.Rcovs[i].Sd.GetResult(),
					rs.XYs[i].Mean.GetN()))
			}
		}
	}

}
